import express from "express"
import { rgbToHex, HexTorgb } from "./converter.js";
import bodyParser from "body-parser"
const app = express()
const port = 3000
// Enable CORS for all routes
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
});
app.use(bodyParser.json());

app.get('/', (req, res) => {
    res.send('Hello World!')
    
})

app.post('/hex-to-rgb', (req, res) => {
    const hexValue = req.body.hexValue
    const result = HexTorgb(hexValue)
    res.json(result)

})
app.post('/rgb-to-hex', (req, res) => {
    const rgbValue = req.body.rgbValue
    const result = rgbToHex(rgbValue)
    console.log(result)
    res.json(result)

})
if (process.env.NODE_ENV === "test") {
    module.exports = {
        app
    }
} else {
    app.listen(port, () => {
        console.log(`Example app listening on port ${port}`)
    })
}