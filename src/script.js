// import express from "express";

let hexInput = document.getElementById("hex");
let rgbInput = document.getElementById("rgb");

const hexButton = document.getElementById("btnHex");
const RGBbutton = document.getElementById("btnRGB"); 

const rectangle = document.querySelector('.rectangle');
const port = 3000
window.onload = () => {
    hexInput.value = "";
    rgbInput.value = "";
}

hexButton.addEventListener('click', ()=>{
    // Make a GET request to the server endpoint
    var endpoint = `http://localhost:${port}/hex-to-rgb`
    if (hexInput.value !== "" ){
        fetch(endpoint, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json', // Set the content type to JSON
                // Add any other headers as needed
            },
            body: JSON.stringify({
                hexValue: hexInput.value
            }) // Convert the data to a JSON string
        }).then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json(); // Parse the response body as JSON
        })
            .then(data => {
                // Update the HTML with the received data
                console.log(data)
                rgbArr = data;
                document.body.style.backgroundColor = rgbArr;
                rectangle.style.backgroundColor = rgbArr;
                hexButton.style.backgroundColor = rgbArr
                RGBbutton.style.backgroundColor = rgbArr
                rgbInput.value = ""
            })
            .catch(error => {
                console.error('Error:', error);
            });
    }
    else{
        window.alert("Please Enter something")
    }

})

RGBbutton.addEventListener("click", ()=> {
    var endpoint = `http://localhost:${port}/rgb-to-hex`
    if (rgbInput.value !== "") {
        fetch(endpoint, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ rgbValue: rgbInput.value })
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                return response.json(); // Parse the response body as JSON
            })
            .then(data => {
                // Update the HTML with the received data
                hex = data;
                console.log(hex)
                document.body.style.backgroundColor = hex;
                rectangle.style.backgroundColor = hex;
                hexButton.style.backgroundColor = hex
                RGBbutton.style.backgroundColor = hex
                hexInput.value = ""
            })
            .catch(SyntaxError => {
                window.alert("Please enter the correct Format: rgb(#,#,#)")
            }); 
    }
    else{
        window.alert("Please Enter something")
    }
 
})
