/**
 * 
 * @param {string} red 
 * @param {string} green 
 * @param {string} blue 
 */

export function HexTorgb(hex){
    hex = hex.replace(/^#/, '');

    // Check if the hex value is a shorthand (e.g., #F00) or a full form (e.g., #FF0000)
    if (hex.length === 3) {
        hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2]; // Expand shorthand
    }

    // Parse the hexadecimal value to get the individual RGB components
    const bigint = parseInt(hex, 16);
    const r = (bigint >> 16) & 255;
    const g = (bigint >> 8) & 255;
    const b = bigint & 255;

    return `rgb(${r}, ${g}, ${b})`;
}

function valid(element) {
    element.style.color = "#202040";
}
function invalid(element, otherElement) {
    element.style.color = "#f04624";
    otherElement.value = 0;
}

export function rgbToHex(rgb) {
    let rgbCode = rgb;
    let rgbRegex1 = /^rgb\([0-9]{1,3},[0-9]{1,3},[0-9]{1,3}\)$/;
    let rgbRegex2 = /^[0-9]{1,3},[0-9]{1,3},[0-9]{1,3}$/
    let hex = "#";
    if (rgbRegex1.test(rgbCode) || rgbRegex2.test(rgbCode)) {
        rgbCode = rgbCode.replace(/[rgb()]+/g, "") || rgbCode;
        rgbCode = rgbCode.split(",");
        let condition = rgbCode.every((value) => {
            return parseInt(value) <= 255;
        });
        if (condition) {
            rgbCode.forEach(value => {
                value = parseInt(value).toString(16);
                hex += value.length == 1 ? "0" + value : value;
            });
            return hex
        }
    }
}